import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockSellComponent } from './stock-sell/stock-sell.component';
import { StockHistoryComponent } from './stock-history/stock-history.component';
import { StockListComponent } from './stock-list/stock-list.component';
import { StockStatusComponent } from './stock-status/stock-status.component';
import { StockBuyComponent } from './stock-buy/stock-buy.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'stock-list'},
  { path: 'stock-buy', component: StockBuyComponent},
  { path: 'stock-history', component: StockHistoryComponent},
  { path: 'stock-list', component: StockListComponent},
  { path: 'stock-sell', component: StockSellComponent},
  { path: 'stock-status', component: StockStatusComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
