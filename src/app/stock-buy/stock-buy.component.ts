import { Component, OnInit } from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-stock-buy',
  templateUrl: './stock-buy.component.html',
  styleUrls: ['./stock-buy.component.css']
})
export class StockBuyComponent implements OnInit {

  TradeList: any=[];

  constructor(
    public restApi: RestApiService
  ) { }

  ngOnInit(): void {
    this.loadTrades()
  }

  loadTrades(){
    return this.restApi.getTrades().subscribe((data: {})=>{
      this.TradeList = data;
    })
  }

}
