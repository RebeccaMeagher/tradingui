import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";


@Component({
  selector: 'app-stock-list',
  templateUrl: './stock-list.component.html',
  styleUrls: ['./stock-list.component.css']
})

export class StockListComponent implements OnInit {

  TradeList: any=[];

  constructor(
    public restApi: RestApiService
  ) { }

  ngOnInit(): void {
    this.loadTrades()
  }

  loadTrades(){
    return this.restApi.getTrades().subscribe((data: {})=>{
      this.TradeList = data;
    })
  }

  deleteTrade(id:any) {
    if (window.confirm('Are you sure, you want to sell stock id: ' + id + '?')){
      this.restApi.deleteTrade(id).subscribe(data => {
        this.loadTrades()
      })
    }
  }

  buyTrade(id:any) {
    if (window.confirm('Are you sure, you want to buy stock id: ' + id + '?')){
      // this.restApi.deleteTrade(id).subscribe(data => {
      //   this.loadTrades()
      // })
      this.loadTrades()
    }
  }

}


