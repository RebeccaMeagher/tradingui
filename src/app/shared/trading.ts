import { Time } from "@angular/common";
//import { time } from "console";

export class Trading {
  constructor(){
    this.id=0;
    this.buyOrSell="";
    this.price=0;
    this.statusCode=0;
    this.stockTicker="";
    this.volume=0;
    this.createdTimestamp=new Date(Date.now());
  }
  id: number;
  buyOrSell: string;
  price: number;
  statusCode: number;
  stockTicker: string;
  volume: number;
  createdTimestamp: Date;
}
