import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule} from '@angular/common/http'

import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StockBuyComponent } from './stock-buy/stock-buy.component';
import { StockSellComponent } from './stock-sell/stock-sell.component';
import { StockListComponent } from './stock-list/stock-list.component';
import { StockHistoryComponent } from './stock-history/stock-history.component';
import { StockStatusComponent } from './stock-status/stock-status.component';

@NgModule({
  declarations: [
    AppComponent,
    StockBuyComponent,
    StockSellComponent,
    StockListComponent,
    StockHistoryComponent,
    StockStatusComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
